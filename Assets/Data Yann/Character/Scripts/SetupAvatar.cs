using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEditor;

/// <summary>
/// Sets up all scripts in applicaiton
/// </summary>
public class SetupAvatar : MonoBehaviour
{
    #region Types for editor
    public enum Gender { Female, Male };
    public enum Race { African, Asian, Caucasian };
    public enum HairColor { Black, Blond, Brown, Red, Bald };
    public enum Height { Small, Average, Tall };
    public enum Weight { Thin, Average, Large };
    public enum OvMode { Color, Blur, BlurColor, ColorRamp };
    public enum BeatNb { OneBeat, TwoBeats };
    public enum Group { HeartRate, Respiration, Combined };
    public enum Condition5 { Visual, Auditory, Tactile, Combined, Nothing };
    public enum Condition3 { HeartRate, Respiration, Combined };
    public enum SkinTone { Light, Medium, Dark };
    #endregion

    #region GameObjects and Components
    private GameObject avatar;
    private GameObject centerEye;

    private PhysiologicalModel physio;
    #endregion

    #region General parameters
    private bool exitRoutineStarted = false;

    [Header("Condition")]
    [Tooltip("Check if first condition (to mirror face mirror at start)")]
    public bool isFirst = true;

    [Header("Avatar appearance")]
    [Tooltip("Gender")]
    public Gender gender = 0;
    [Tooltip("Race")]
    public Race race = 0;
    [Tooltip("HairColor")]
    public HairColor hairColor = 0;
    [Tooltip("Skin tone")]
    public SkinTone skinTone = 0;
    [Tooltip("Height (depends on Gender and Race)")]
    public Height height = 0;
    [Tooltip("Weight")]
    public Weight weight = 0;

    
    #endregion

    #region Animation script parameters
    [Header("Animation script parameters")]
    [Tooltip("Speed for walking animation")]
    public float animSpeed = 0;
    [Tooltip("Overall simulation duration (s)")]
    public float duration = 150;
    [Tooltip("Static time before and after walking (s)")]
    public float timeOffWalking = 20;
    [Tooltip("How much the slope reduces the walking speed")]
    public float slopeToSpeed = 1;
    [Tooltip("Layer for avatar raycasting")]
    public LayerMask ground;
    [Tooltip("Damping factor for swaying motion")]
    public float dampSwaying = 0.01f;
    [Tooltip("Traces line to highlight eye position")]
    public bool debugEyePosition = true;
    private Vector3 startPos = Vector3.zero;
    #endregion

    #region Respiration parameters
    [Header("Respiration parameters")]
    [Tooltip("Show chest movement")]
    public bool showChestMovement = true;
    [Tooltip("Play breathing sound")]
    public bool playBreathingSound = true;
    [Tooltip("Force faster breathing (not accurate physiological model)")]
    public float accBreathingFactor = 3;
    #endregion

    #region Physiological model parameters
    [Header("Override avatar parameters")]
    [Tooltip("Use other parameters for physiological model")]
    public bool overridePhysiologicalModel = false;
    [Tooltip("Gender")]
    public Gender genderOv = 0;
    [Tooltip("Race")]
    public Race raceOv = 0;
    [Tooltip("Height (depends on Gender and Race)")]
    public Height heightOv = 0;
    [Tooltip("Weight")]
    public Weight weightOv = 0;

    [Header("Physiological model parameters")]
    [Tooltip("Age (years)")]
    public int age = 30;
    [Tooltip("Fitness(0 for not fit, to 7 for fit)")]
    public float fitness = 3.5f;
    [Tooltip("Minimum heart rate (bpm)")]
    public float HRmin = 80f;
    [Tooltip("Factor to compute weight from weight index")]
    public float corpulenceFact = 0.5f;
    [Tooltip("Time for effort to reach 95% of target (s)")]
    public float physioTimeTo95Per = 10;        // TODO
    [Tooltip("Tidal volume calculation mode (smooth or not)")]
    public bool physioTVSmooth = true;          // TEMP
    [Tooltip("Effort calculation : slope cosine to VO2")]
    public float physioSlopeToVO2 = 6;          // TODO
    [Tooltip("Effort calculation : speed to VO2")]
    public float physioSpeedToVO2 = 36;         // TODO
    public float[] precompBR = { 10, 12, 19.5f, 27, 10 };
    #endregion

    #region Sounds parameters
    [Header("Sounds parameters")]
    [Tooltip("Footstep audio clips (exterior)")]
    public AudioClip[] stepSoundsExt;
    [Tooltip("AudioMixerGroup for step sounds")]
    public AudioMixerGroup stepAMG;
    [Tooltip("Exterio snapshot")]
    public AudioMixerSnapshot extSnap;
    #endregion

    #region Gender, Race, Height - related parameters
    //public enum Group { HeartRate, Respiration, Combined };
    public string[] grpToStr = { "h", "r", "c" };
    //public enum Condition5 { Visual, Auditory, Tactile, Combined, Nothing };
    public string[] cond5ToStr = { "v", "a", "t", "c", "r" };
    //public enum Condition3 { HeartRate, Respiration, Combined };
    public string[] cond3ToStr = { "h", "r", "c" };
    // Gender, Race, Height and Weight equivalents in String, String, cm, and %BMI
    private char[] genders = new char[] { 'F', 'M' };
    private string[] races = new string[] { "AFR", "ASI", "COC" };
    public int[] heights = new int[] { 157, 150, 155, 170, 160, 165 };
    private int[] weights = new int[] { 80, 100, 125 };

    // Offset from chest1 to heart (to place sound emitter)
    private Vector3[] heartOffsets = new Vector3[]
    {
        new Vector3(0.001f, -0.013f, 0.097f),
        new Vector3(0.001f, -0.013f, 0.097f),
        new Vector3(0.001f, -0.013f, 0.097f),
        new Vector3(-0.002f, 0.073f, 0.145f),
        new Vector3(-0.001f, 0.078f, 0.145f),
        new Vector3(-0.001f, 0.058f, 0.16f)
    };
    // Offset from feet to groud : copied into IK solver in avatar prefabs
    private float[] feetOffsets = new float[]
    {
        0.19f, 0.19f, 0.19f,
        0.17f, 0.19f, 0.20f,
        0.18f, 0.19f, 0.20f,
        0.26f, 0.27f, 0.28f,
        0.23f, 0.24f, 0.255f,
        0.25f, 0.26f, 0.27f,
    };
    // Hair mesh names
    private string[] hairMeshNames = new string[]
    {
        "afro01Mesh",
        "ponytail01Mesh",
        "ponytail01Mesh",
        "",
        "short01Mesh",
        "short01Mesh"
    };
    // Offset from avatar position to camera
    public Vector3[] centerEyeOffset = new Vector3[]
    {
        new Vector3(0, -0.142f, -0.076f),
        new Vector3(0, -0.142f, -0.080f),
        new Vector3(0, -0.148f, -0.093f),
        new Vector3(0, -0.080f, -0.075f),
        new Vector3(0, -0.133f, -0.085f),
        new Vector3(0, -0.135f, -0.090f),
        new Vector3(0, -0.135f, -0.075f),
        new Vector3(0, -0.140f, -0.090f),
        new Vector3(0, -0.150f, -0.100f),
        new Vector3(0, -0.150f, -0.110f),
        new Vector3(0, -0.150f, -0.115f),
        new Vector3(0, -0.165f, -0.125f),
        new Vector3(0, -0.145f, -0.120f),
        new Vector3(0, -0.155f, -0.125f),
        new Vector3(0, -0.160f, -0.130f),
        new Vector3(0, -0.150f, -0.130f),
        new Vector3(0, -0.155f, -0.138f),
        new Vector3(0, -0.160f, -0.140f)
    };
    // Skin materials
    public Material[] skinMats = new Material[] { };
    // Hair materials
    public Material[] hairMats = new Material[] { };
    // Timings used to compute scale env for simulation to last same time for everyone
    private float[] scaleEnv = new float[]
    {
        123, 115, 108,
        123, 117, 110,
        123, 115, 109,
        111, 104, 99,
        120, 113, 107,
        115, 108, 102,
    };
    #endregion

    #region Time sequence parameters
    [Header("Timing parameters")]
    [Tooltip("Time before displaying physiological feedback (s)")]
    public float timeBeforePhysio = 5;
    [Tooltip("Time before starting to walk with physiological feedback on (s)")]
    public float timeBeforeWalking = 5;
    [Tooltip("Time before leaving the room in frist condition (s)")]
    public float timeInRoom = 60;
    [Tooltip("Time before starting physiological feedback after setting band (s)")]
    public float deltaAfterBandSet = 1;
    private bool isPhysioPlaying = false;
    #endregion

    /// <summary>
    /// Called once at start
    /// </summary>
    void Start()
    {
        ScaleEnv();
        CreateAvatar();
        SetJogAnimator();
        SetPhysiologicalModel();
        SetCamera();
        SetAnimationScript();
        SetStepSoundEmitters();
        SetBreathingComponents();
    }

    /// <summary>
    /// Called once per frame
    /// </summary>
    void Update()
    {
        // Trace line that stays on screen to show eye position over time
        if (debugEyePosition)
        {
            Vector3 p = centerEye.transform.position;
            Debug.DrawLine(p, p + Vector3.down * 2, Color.red, 10, false);
        }

        // Start physiological feedback when necessary, then start walking
        float delay = isFirst ? timeInRoom - timeBeforePhysio : timeBeforePhysio;
        if (!isPhysioPlaying && Time.time > delay)
        {
            StartCoroutine(StartPhysio(deltaAfterBandSet));
            isPhysioPlaying = true;
        }
    }



    /// <summary>
    /// Start physiological feedback
    /// </summary>
    /// <param name="delay">Delay in s</param>
    /// <returns>Coroutine for starting to walk after a delay</returns>
    IEnumerator StartPhysio(float delay)
    {
        yield return new WaitForSeconds(delay);

        StartCoroutine(StartWalking(timeBeforeWalking));
    }

    /// <summary>
    /// Start walking after a delay
    /// </summary>
    /// <param name="delay">Delay in s</param>
    /// <returns>Coroutine for starting to walk after a delay</returns>
    IEnumerator StartWalking(float delay)
    {
        yield return new WaitForSeconds(delay);
        Debug.Log("Start Walking");
        animSpeed = 1.5f;

        if (isFirst)
        {
            GameObject.Find("Door").GetComponent<Animator>().SetTrigger("OpenDoor");
            extSnap.TransitionTo(3);
        }
    }

    /// <summary>
    /// Compute avatar prefab name from parameters
    /// </summary>
    /// <param name="gender">Gender</param>
    /// <param name="race">Race</param>
    /// <param name="height">Height</param>
    /// <param name="weight">Weight</param>
    /// <param name="age">Add age to name if enabled</param>
    /// <returns>Prefab name</returns>
    private string ComputePrefabName(Gender gender, Race race, Height height, Weight weight, bool age)
    {
        string prefabName = "";
        prefabName += genders[(int)gender];
        prefabName += races[(int)race];
        if (age) prefabName += "A32";
        prefabName += "H" + (heights[(int)gender * 3 + (int)race] + 10 * (int)height);
        prefabName += "W" + weights[(int)weight];

        return prefabName;
    }



    /// <summary>
    /// Scale environment for simulation to last same duration for each size of avatar
    /// </summary>
    private void ScaleEnv()
    {
        // Compute scaling factor
        float factor = (duration - timeOffWalking) / scaleEnv[(int)gender * 9 + (int)race * 3 + (int)height];
        // Scale environment
        GameObject.Find("Ground").transform.localScale *= factor;       // Ground
        GameObject grass = GameObject.Find("GrassAndTrees");            // Grass, flowers, bushes, trees
        grass.GetComponent<Terrain>().terrainData.size *= factor;
        grass.transform.position = new Vector3(-112.5f * factor, 0, -180 * factor);
        GameObject.Find("SceneMarkers").transform.localScale *= factor; // Scene markers
        GameObject.Find("Signs").transform.localScale *= factor;        // Sign posts

        if (!isFirst) startPos *= factor;
    }

    /// <summary>
    /// Create avatar instance
    /// </summary>
    private void CreateAvatar()
    {
        // Create avatar
        string prefabName = "AvatarModels/" + ComputePrefabName(gender, race, height, weight, false);
        avatar = Instantiate(Resources.Load(prefabName)) as GameObject;
        avatar.name = "AvatarModel";

        // Hair color
        string hairMeshName = hairMeshNames[(int)gender * 3 + (int)race];
        if (hairColor == HairColor.Bald)
            Destroy(GameObject.Find(hairMeshName));
        else if (race != Race.African)
            GameObject.Find(hairMeshName).GetComponent<SkinnedMeshRenderer>().material = hairMats[(int)gender * 4 + (int)hairColor];

        // Transform
        avatar.transform.position = startPos;
        avatar.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        avatar.transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    /// <summary>
    /// Attach animator to avatar
    /// </summary>
    private void SetJogAnimator()
    {
        // Jog animation
        Animator anim = avatar.GetComponent<Animator>();
        anim.runtimeAnimatorController = Resources.Load("Components/JoggingController") as RuntimeAnimatorController;
    }

    /// <summary>
    /// Initialize physiological model
    /// </summary>
    private void SetPhysiologicalModel()
    {
        int gd = (int)(overridePhysiologicalModel ? genderOv : gender);
        int rc = (int)(overridePhysiologicalModel ? raceOv : race);
        int ht = (int)(overridePhysiologicalModel ? heightOv : this.height);
        int wt = (int)(overridePhysiologicalModel ? weightOv : weight);


        // Physiological model
        physio = avatar.AddComponent<PhysiologicalModel>();
        float height = (heights[gd * 3 + rc] + 10 * ht) / 100f;
        physio.Initialize(
            genders[gd],
            height,
            21.75f * ((weights[wt] - 100f) * corpulenceFact * 0.01f + 1f) * height * height,
            age,
            fitness,
            precompBR
        );
    }

    /// <summary>
    /// Place camera
    /// </summary>
    private void SetCamera()
    {
        // Set up marker at eyes level for camera
        centerEye = new GameObject("CenterEye");

        // Set up objects to hide for person Camera
        GameObject camera = GameObject.Find("Camera");
        RenderHide rh = camera.GetComponent<RenderHide>();
        string skinMeshName = ComputePrefabName(gender, race, height, weight, true).ToLower() + "Mesh";
        string hairMeshName = (hairColor == HairColor.Bald) ? "" : hairMeshNames[(int)gender * 3 + (int)race];
        GameObject[] gos = new GameObject[hairMeshName == "" ? 2 : 3];
        gos[0] = avatar.transform.Find("high-polyMesh").gameObject;
        gos[1] = avatar.transform.Find("eyebrow012Mesh").gameObject;
        if (hairMeshName != "") gos[2] = avatar.transform.Find(hairMeshName).gameObject;
        rh.Initialize(avatar.transform.Find(skinMeshName).GetComponent<SkinnedMeshRenderer>(), gos, skinMats[(int)gender * 3 + (int)skinTone]);

        // Attached Camera with the centerEye
        GameObject carmeraRig = GameObject.Find("[CameraRig]");
        carmeraRig.GetComponent<CalibrateCenter>().centerEye = centerEye.transform;
        carmeraRig.transform.parent = centerEye.transform;
        carmeraRig.transform.localPosition = Vector3.zero;

        // Set up parameter to avoid disappearing shoes in camera
        avatar.transform.Find("shoes05Mesh").GetComponent<SkinnedMeshRenderer>().updateWhenOffscreen = true;
    }

    /// <summary>
    /// Initialize animation script
    /// </summary>
    private void SetAnimationScript()
    {
        AnimationScript animScript = avatar.AddComponent<AnimationScript>();
        float h = (heights[(int)gender * 3 + (int)race] + 10 * (int)height) / 100f;
        animScript.SetParams(
             new Vector3(0, h, 0) + centerEyeOffset[(int)gender * 9 + (int)race * 3 + (int)height],
             feetOffsets[(int)gender * 9 + (int)race * 3 + (int)height],
             h
        );
    }

    /// <summary>
    /// Set sound sources for steps
    /// </summary>
    private void SetStepSoundEmitters()
    {
        AudioSource left = GameObject.Find("foot.L").AddComponent<AudioSource>();
        left.spatialBlend = 1;
        left.maxDistance = 4;
        left.minDistance = 3;
        left.outputAudioMixerGroup = stepAMG;
        left.playOnAwake = false;
        AudioSource right = GameObject.Find("foot.R").AddComponent<AudioSource>();
        right.spatialBlend = 1;
        right.maxDistance = 4;
        right.minDistance = 3;
        right.outputAudioMixerGroup = stepAMG;
        right.playOnAwake = false;
    }

    /// <summary>
    /// Set breathing components
    /// </summary>
    private void SetBreathingComponents()
    {
        // Breathing animator
        Animator breathingAnim = GameObject.Find("Suit").AddComponent<Animator>();

    }

    /// <summary>
    /// Set heartbeat components
    /// </summary>
    private void SetHeartbeatComponents()
    {
    }

    /// <summary>
    /// Trigger exit routine
    /// </summary>
    public void ExitRoutine()
    {
        if (!exitRoutineStarted)
        {
            exitRoutineStarted = true;
            EditorApplication.ExitPlaymode();
        }
    }
}
