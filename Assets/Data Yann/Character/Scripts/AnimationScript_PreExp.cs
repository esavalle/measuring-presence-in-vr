﻿using UnityEngine;

/// <summary>
/// Handles all walking animation - related actions
/// </summary>
public class AnimationScript_PreExp : MonoBehaviour
{
    #region Parameters
    // Components
    private Animator anim;
    private SetupAvatar_PreExp setupAvatar;

    // Factor used to take local scale into account
    private float scaleFactor;
    
    // Animation
    private bool isIdle = false;
    private float idleZPos;

    // Head rotation
    private Transform eyeL;
    private Transform eyeR;
    private Transform cameraTf;
    private Transform lookAtEffector;

    // TODO : Offsets
    private float height;
    private float raycastOffset = 0.15f;    // Offset launch raycast
    private float feetOffset;               // In case it's used (hard coded in avatar components)
    // Offset to account for character posture on slope (* slopeSine * avatar height)
    private float centerEyeOffsetIKRel = 0.05f;
    // Forward offset to account for head lootAtIK (* avatar height)

    // Camera position
    private Transform centerEye;
    private Vector3 centerEyeOffset;

    // Step variables
    private float stepDelay = 0.1f;
    private AudioSource leftStepSoundEmitter;
    private AudioSource rightStepSoundEmitter;
    private float lastStepTimeL = 0;
    private float lastStepTimeR = 0;
    #endregion

    /// <summary>
    /// Called once on start
    /// </summary>
    void Start()
    {
        setupAvatar = GameObject.Find("SINGLETON").GetComponent<SetupAvatar_PreExp>();
        scaleFactor = transform.localScale.x / 0.3f;
        anim = gameObject.GetComponent<Animator>();
        anim.SetFloat("Speed", setupAvatar.animSpeed);
        leftStepSoundEmitter = GameObject.Find("foot.L").GetComponent<AudioSource>();
        rightStepSoundEmitter = GameObject.Find("foot.R").GetComponent<AudioSource>();
        cameraTf = GameObject.Find("Camera").transform;
        eyeL = GameObject.Find("eye.L").transform;
        eyeR = GameObject.Find("eye.R").transform;
        centerEye = GameObject.Find("CenterEye").transform;
        lookAtEffector = GameObject.Find("LookAtEffector").transform;
    }

    /// <summary>
    /// Check if end reached,
    /// Raycast for physiological model + move avatar upwards + reduce speed
    /// </summary>
    void Update()
    {
        CheckEnd();
        CheckSlope();
        anim.SetFloat("Speed", setupAvatar.animSpeed);
    }

    /// <summary>
    /// Move camera forward + swaying motion + rotate head to match camera
    /// </summary>
    private void LateUpdate()
    {
        // Handle z pos in idle animation
        string clip = anim.GetCurrentAnimatorClipInfo(0)[0].clip.name;
        if (clip == "MIdle" || clip == "FIdle")
        {
            if (!isIdle)
            {
                idleZPos = transform.position.z;
                isIdle = true;
            }
            transform.position = new Vector3(transform.position.x, transform.position.y, idleZPos);
        }
        else
            isIdle = false;

        // Camera follows character
        centerEye.position =
            transform.position +
            centerEyeOffset +
            Vector3.forward * centerEyeOffsetIKRel * height;

        // Slight swaying motion
        Vector3 pos = centerEye.position;
        pos.y = centerEye.position.y + (eyeL.position.y - centerEye.position.y) * setupAvatar.dampSwaying;
        centerEye.position = pos;

        // Stick head rotation to camera rotation
        lookAtEffector.position = (eyeL.position + eyeR.position) / 2 + cameraTf.forward;
    }

    /// <summary>
    /// Stop walking at end marker, then exit application
    /// </summary>
    private void CheckEnd()
    {
        if (transform.position.z < -166.5f)
        {
            transform.position = new Vector3(
                transform.position.x,
                transform.position.y,
                0);
        }
    }

    /// <summary>
    /// Check slope under character
    /// </summary>
    private void CheckSlope()
    {
        // Cast ray to ground under character center
        RaycastHit hitInfo;
        if (Physics.Raycast(
            transform.position + Vector3.up * 3 * scaleFactor,
            -Vector3.up,
            out hitInfo,
            6 * scaleFactor,
            setupAvatar.ground))
        {
            // Maintain constant distance character / ground + Have right steated position
            transform.position += new Vector3(0, (3 + raycastOffset) * scaleFactor - hitInfo.distance, 0);
        }
        else
        {
            Debug.Log("no ground");
        }
    }

    /// <summary>
    /// Play step sound (called from animation events)
    /// </summary>
    /// <param name="cmd">cmd / 100 gives foot side (0 for left, 100 for right) - cmd % 100 gives volume multiplier (10 for 100% volume, 3 for 30%)</param>
    public void StepEvent(int cmd)
    {
        int side = cmd / 100;

        // choose proper sound bank accroding to feets position (wood for interior, earth for exterior)
        AudioClip[] clips;
        float mult = (float)(cmd % 100) / 10;
        clips = setupAvatar.stepSounds;
        mult *= 0.5f;

        AudioClip c = clips[Random.Range(0, clips.Length)];

        // play sound right on foot
        if (side == 0 && Time.time - lastStepTimeL > stepDelay)
        {
            lastStepTimeL = Time.time;
            leftStepSoundEmitter.clip = c;
            leftStepSoundEmitter.volume = mult;
            leftStepSoundEmitter.Play();
        }
        else if (side == 1 && Time.time - lastStepTimeR > stepDelay)
        {
            lastStepTimeR = Time.time;
            rightStepSoundEmitter.clip = c;
            rightStepSoundEmitter.volume = mult;
            rightStepSoundEmitter.Play();
        }
    }

    /// <summary>
    /// Set offset parameters
    /// </summary>
    /// <param name="centerEyeOffset">Offset between transform position and eyes center</param>
    /// <param name="feetOffset"Offset>Offset for feet placement on ground (read only, value is hard coded in avatar GrounderFullBodyBiped components)</param>
    /// <param name="height">Avatar height</param>
    public void SetParams(Vector3 centerEyeOffset, float feetOffset, float height)
    {
        this.centerEyeOffset = centerEyeOffset;
        this.feetOffset = feetOffset;
        this.height = height;
    }
}
