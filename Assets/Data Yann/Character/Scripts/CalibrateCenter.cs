using System.Collections;
using UnityEngine;

/// <summary>
/// Moves camera between the eyes
/// </summary>
public class CalibrateCenter : MonoBehaviour
{
    // GameObject and Components
    public Transform centerEye;
    public GameObject cam;

    // General parameters
    public Vector3 eyeCenter;
    public Vector3 eyeDirection;
    private bool centerEyeGood = false;

    /// <summary>
    /// Called once per frame
    /// </summary>
    void Update()
    {
        // Search for center eye marker in the scene
        if (centerEye == null)
        {
            centerEye = GameObject.Find("CenterEye").transform;
            centerEyeGood = true;
        }

        // Move the camera at the center eye location
        if (centerEyeGood == true)
        {
            eyeCenter = centerEye.position;
            eyeDirection = centerEye.forward;

            StartCoroutine(Calibrate());
            enabled = false; // Stop this update function
        }
    }

    /// <summary>
    /// Moves camera at proper position
    /// </summary>
    /// <returns>Coroutine that moves camera</returns>
    IEnumerator Calibrate()
    {
        yield return new WaitForEndOfFrame();

        Vector3 userForward = Camera.main.transform.forward;
        userForward.y = 0.0f;
        userForward.Normalize();

        transform.rotation = Quaternion.FromToRotation(userForward, eyeDirection);

        Vector3 userPos = Camera.main.transform.position;
        eyeCenter.y = 0.0f;
        userPos.y = 0.0f;

        transform.position = eyeCenter - userPos;
    }
}
