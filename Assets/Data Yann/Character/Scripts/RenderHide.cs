using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// Hides head when rendering camera
/// </summary>
[RequireComponent(typeof(Camera))]
public class RenderHide : MonoBehaviour
{
    // GameObjects and components
    private SetupAvatar avatar;
    private SkinnedMeshRenderer smRenderer; // SkinnedMeshRenderer for skin
    private GameObject[] objectsToHide;     // Objects to hide from VR camera
    private Material skinMat;
    
    // Materials
    public Material transparentMaterial;    // Material which will replace the original skin material
    Material originalMaterial;              // Use to back up the original skin material

    /// <summary>
    /// Called before camera rendering : hide stuff
    /// </summary>
    void OnPreRender()
    {
        if (!smRenderer) return;

        smRenderer.sharedMaterial = transparentMaterial;

        foreach (GameObject o in objectsToHide)
            o.GetComponent<SkinnedMeshRenderer>().shadowCastingMode = ShadowCastingMode.ShadowsOnly;
    }

    /// <summary>
    /// Called after camera rendering : show stuff again
    /// </summary>
    void OnPostRender()
    {
        if (!smRenderer) return;

        foreach (GameObject o in objectsToHide)
            o.GetComponent<SkinnedMeshRenderer>().shadowCastingMode = ShadowCastingMode.On;

        smRenderer.sharedMaterial = originalMaterial;
        smRenderer.material = skinMat;
    }

    /// <summary>
    /// Called once at start by SetupAvatar
    /// </summary>
    /// <param name="smRenderer">SkinnedMeshRenderer for avatar skin</param>
    /// <param name="objectsToHide">Objects to hide from camera</param>
    /// <param name="skinMat">Skin material</param>
    public void Initialize(SkinnedMeshRenderer smRenderer, GameObject[] objectsToHide, Material skinMat)
    {
        this.smRenderer = smRenderer;
        this.objectsToHide = objectsToHide;
        this.skinMat = skinMat;

        avatar = GameObject.Find("SINGLETON").GetComponent<SetupAvatar>();
        
        originalMaterial = this.skinMat;

        transparentMaterial.mainTexture = originalMaterial.mainTexture;
        transparentMaterial.SetFloat("_Glossiness", originalMaterial.GetFloat("_Glossiness"));
        transparentMaterial.SetFloat("_Metallic", originalMaterial.GetFloat("_Metallic"));
    }
}
