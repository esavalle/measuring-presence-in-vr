using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEditor;
using Valve.VR;

/// <summary>
/// Sets up all scripts in applicaiton
/// </summary>
public class SetupAvatar_PreExp : MonoBehaviour
{
    #region Types for editor
    public enum Group { Group1, Group2 };
    public enum Gender { Female, Male };
    public enum Race { African, Asian, Caucasian };
    public enum HairColor { Black, Blond, Brown, Red, Bald };
    public enum Height { Small, Average, Tall };
    public enum Weight { Thin, Average, Large };
    public enum SkinTone { Light, Medium, Dark };
    #endregion

    #region GameObjects and Components
    private GameObject avatar;
    private GameObject centerEye;
    #endregion

    #region General parameters
    private bool exitRoutineStarted = false;
    private int state = 0;
    private float timeFirstCond = -1;

    public Group group;

    [Tooltip("Wrap when inpire or expire")]
    public bool wrapInspire = true;
    private bool prevWrapInspire;

    [Header("Avatar appearance")]
    [Tooltip("Gender")]
    public Gender gender = 0;
    [Tooltip("Race")]
    public Race race = 0;
    [Tooltip("HairColor")]
    public HairColor hairColor = 0;
    [Tooltip("Skin tone")]
    public SkinTone skinTone = 0;
    [Tooltip("Height (depends on Gender and Race)")]
    public Height height = 0;
    [Tooltip("Weight")]
    public Weight weight = 0;
    #endregion

    #region Animation script parameters
    [Header("Animation script parameters")]
    [Tooltip("Speed for walking animation")]
    public float animSpeed = 0;
    [Tooltip("Overall simulation duration (s)")]
    public float duration = 150;
    [Tooltip("Static time before and after walking (s)")]
    public float timeOffWalking = 20;
    [Tooltip("How much the slope reduces the walking speed")]
    public float slopeToSpeed = 1;
    [Tooltip("Layer for avatar raycasting")]
    public LayerMask ground;
    [Tooltip("Damping factor for swaying motion")]
    public float dampSwaying = 0.01f;
    [Tooltip("Traces line to highlight eye position")]
    public bool debugEyePosition = true;
    public Transform position;
    private Vector3 startPos = Vector3.zero;
    #endregion

    #region Respiration parameters
    [Range(0, 1)]
    public float debugBreathingFact = 0;
    public float BRRest = 10;
    public float BR;
    public float TV = 0;
    private float BRMax = 27;
    private float TVMax = 0.9f;
    #endregion

    #region Sounds parameters
    [Header("Sounds parameters")]
    [Tooltip("Footstep audio clips (exterior)")]
    public AudioClip[] stepSounds;
    [Tooltip("AudioMixerGroup for step sounds")]
    public AudioMixerGroup stepAMG;
    [Tooltip("Exterio snapshot")]
    public AudioMixerSnapshot extSnap;
    #endregion

    #region Gender, Race, Height - related parameters
    // Gender, Race, Height and Weight equivalents in String, String, cm, and %BMI
    private char[] genders = new char[] { 'F', 'M' };
    private string[] races = new string[] { "AFR", "ASI", "COC" };
    public int[] heights = new int[] { 157, 150, 155, 170, 160, 165 };
    private int[] weights = new int[] { 80, 100, 125 };

    // Offset from feet to groud : copied into IK solver in avatar prefabs
    private float[] feetOffsets = new float[]
    {
        0.19f, 0.19f, 0.19f,
        0.17f, 0.19f, 0.20f,
        0.18f, 0.19f, 0.20f,
        0.26f, 0.27f, 0.28f,
        0.23f, 0.24f, 0.255f,
        0.25f, 0.26f, 0.27f,
    };
    // Hair mesh names
    private string[] hairMeshNames = new string[]
    {
        "afro01Mesh",
        "ponytail01Mesh",
        "ponytail01Mesh",
        "",
        "short01Mesh",
        "short01Mesh"
    };
    // Offset from avatar position to camera
    private Vector3[] centerEyeOffset = new Vector3[]
    {
        new Vector3(0, -0.142f, -0.076f),
        new Vector3(0, -0.142f, -0.080f),
        new Vector3(0, -0.148f, -0.093f),
        new Vector3(0, -0.080f, -0.075f),
        new Vector3(0, -0.133f, -0.085f),
        new Vector3(0, -0.135f, -0.090f),
        new Vector3(0, -0.135f, -0.075f),
        new Vector3(0, -0.140f, -0.090f),
        new Vector3(0, -0.150f, -0.100f),
        new Vector3(0, -0.150f, -0.110f),
        new Vector3(0, -0.150f, -0.115f),
        new Vector3(0, -0.165f, -0.125f),
        new Vector3(0, -0.145f, -0.120f),
        new Vector3(0, -0.155f, -0.125f),
        new Vector3(0, -0.160f, -0.130f),
        new Vector3(0, -0.150f, -0.130f),
        new Vector3(0, -0.155f, -0.138f),
        new Vector3(0, -0.160f, -0.140f)
    };
    // Skin materials
    public Material[] skinMats = new Material[] { };
    // Hair materials
    public Material[] hairMats = new Material[] { };
    #endregion

    #region Time sequence parameters
    [Header("Timing parameters")]
    [Tooltip("Time before displaying physiological feedback (s)")]
    public float timeBeforePhysio = 5;
    [Tooltip("Time before starting to walk with physiological feedback on (s)")]
    public float timeBeforeWalking = 5;
    [Tooltip("Time before starting physiological feedback after setting band (s)")]
    public float deltaAfterBandSet = 1;
    #endregion

    private bool launchMovement = false;
    private bool endMovement = false;
    private bool isMovementStarted = false;
    /// <summary>
    /// Called once at start
    /// </summary>
    void Start()
    {
        wrapInspire = group != Group.Group1;
        prevWrapInspire = wrapInspire;

        startPos = Vector3.zero;
        extSnap.TransitionTo(0.5f);
        BR = BRRest;
        TV = 0;
        if (position != null) { startPos = position.position; }
        CreateAvatar();
        SetJogAnimator();
        SetCamera();
        SetAnimationScript();
        SetStepSoundEmitters();
        SetBreathingComponents();
    }

    /// <summary>
    /// Called once per frame
    /// </summary>
    void Update()
    {
        // Trace line that stays on screen to show eye position over time
        if (debugEyePosition)
        {
            Vector3 p = centerEye.transform.position;
            Debug.DrawLine(p, p + Vector3.down * 2, Color.red, 10, false);
        }

        // Update BR and TV
        BR = debugBreathingFact * (BRMax - BRRest) + BRRest;
        TV = debugBreathingFact * TVMax;

        // Start physiological feedback when necessary, then start walking
        float delay = timeBeforePhysio;

        // change from first to second
        if (timeFirstCond != -1 && state == 0 && Time.time >= timeFirstCond + 60 + deltaAfterBandSet)
        {
            wrapInspire = !wrapInspire;
            state++;
            Debug.Log("Changed condition!");
        }

        // debug log time to give the controller
        if (timeFirstCond != -1 && state == 1 && Time.time >= timeFirstCond + 120 + deltaAfterBandSet)
        {
            state++;
            Debug.Log("GIVE THE CONTROLLER!");
        }

        if(launchMovement == true && isMovementStarted == false)
        {
            isMovementStarted = true;
            launchMovement = false;
            StartCoroutine(StartWalking(0));
        }
        if(endMovement == true && isMovementStarted == true)
        {
            endMovement = false;
            isMovementStarted = false;
            StopCoroutine(StartWalking(0));
            StartCoroutine(StopWalking(0));
        }
    }

    /// <summary>
    /// Called at end of simulation
    /// </summary>
    private void OnApplicationQuit()
    {
        // Print time at end
        Debug.Log("Ended after " + Time.time + " seconds");
    }

    /// <summary>
    /// Start physiological feedback
    /// </summary>
    /// <param name="delay">Delay in s</param>
    /// <returns>Coroutine for starting to walk after a delay</returns>
    IEnumerator StartPhysio(float delay)
    {
        yield return new WaitForSeconds(delay);

        StartCoroutine(StartWalking(timeBeforeWalking));
    }

    /// <summary>
    /// Start walking after a delay
    /// </summary>
    /// <param name="delay">Delay in s</param>
    /// <returns>Coroutine for starting to walk after a delay</returns>
    IEnumerator StartWalking(float delay)
    {
        yield return new WaitForSeconds(delay);

        animSpeed = 1.45f;
        //animSpeed = 1f;
    }
    IEnumerator StopWalking(float delay)
    {
        yield return new WaitForSeconds(delay);

        animSpeed = 0f;
    }
    public void startMovement()
    {
        launchMovement = true;
    }

    public void stopMovement()
    {
        endMovement = true;

    }
    /// <summary>
    /// Compute avatar prefab name from parameters
    /// </summary>
    /// <param name="gender">Gender</param>
    /// <param name="race">Race</param>
    /// <param name="height">Height</param>
    /// <param name="weight">Weight</param>
    /// <param name="age">Add age to name if enabled</param>
    /// <returns>Prefab name</returns>
    private string ComputePrefabName(Gender gender, Race race, Height height, Weight weight, bool age)
    {
        string prefabName = "";
        prefabName += genders[(int)gender];
        prefabName += races[(int)race];
        if (age) prefabName += "A32";
        prefabName += "H" + (heights[(int)gender * 3 + (int)race] + 10 * (int)height);
        prefabName += "W" + weights[(int)weight];

        return prefabName;
    }

    /// <summary>
    /// Create avatar instance
    /// </summary>
    private void CreateAvatar()
    {
        // Create avatar
        string prefabName = "AvatarModels/" + ComputePrefabName(gender, race, height, weight, false);
        avatar = Instantiate(Resources.Load(prefabName)) as GameObject;
        avatar.name = "AvatarModel";

        // Hair color
        string hairMeshName = hairMeshNames[(int)gender * 3 + (int)race];
        if (hairColor == HairColor.Bald)
            Destroy(GameObject.Find(hairMeshName));
        else if (race != Race.African)
            GameObject.Find(hairMeshName).GetComponent<SkinnedMeshRenderer>().material = hairMats[(int)gender * 4 + (int)hairColor];

        // Transform
        avatar.transform.position = startPos;
        avatar.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        avatar.transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    /// <summary>
    /// Attach animator to avatar
    /// </summary>
    private void SetJogAnimator()
    {
        // Jog animation
        Animator anim = avatar.GetComponent<Animator>();
        anim.runtimeAnimatorController = Resources.Load("Components/JoggingController") as RuntimeAnimatorController;
    }

    /// <summary>
    /// Place camera
    /// </summary>
    private void SetCamera()
    {
        // Set up marker at eyes level for camera
        centerEye = new GameObject("CenterEye");

        // Set up objects to hide for person Camera
        GameObject camera = GameObject.Find("Camera");
        RenderHide rh = camera.GetComponent<RenderHide>();
        string skinMeshName = ComputePrefabName(gender, race, height, weight, true).ToLower() + "Mesh";
        string hairMeshName = (hairColor == HairColor.Bald) ? "" : hairMeshNames[(int)gender * 3 + (int)race];
        GameObject[] gos = new GameObject[hairMeshName == "" ? 2 : 3];
        gos[0] = avatar.transform.Find("high-polyMesh").gameObject;
        gos[1] = avatar.transform.Find("eyebrow012Mesh").gameObject;
        if (hairMeshName != "") gos[2] = avatar.transform.Find(hairMeshName).gameObject;
        rh.Initialize(avatar.transform.Find(skinMeshName).GetComponent<SkinnedMeshRenderer>(), gos, skinMats[(int)gender * 3 + (int)skinTone]);

        // Attached Camera with the centerEye
        GameObject carmeraRig = GameObject.Find("[CameraRig]");
        carmeraRig.GetComponent<CalibrateCenter>().centerEye = centerEye.transform;
        carmeraRig.transform.parent = centerEye.transform;
        carmeraRig.transform.localPosition = Vector3.zero;

        // Set up parameter to avoid disappearing shoes in camera
        avatar.transform.Find("shoes05Mesh").GetComponent<SkinnedMeshRenderer>().updateWhenOffscreen = true;
    }

    /// <summary>
    /// Initialize animation script
    /// </summary>
    private void SetAnimationScript()
    {
        AnimationScript_PreExp animScript = avatar.AddComponent<AnimationScript_PreExp>();
        float h = (heights[(int)gender * 3 + (int)race] + 10 * (int)height) / 100f;
        animScript.SetParams(
             new Vector3(0, h, 0) + centerEyeOffset[(int)gender * 9 + (int)race * 3 + (int)height],
             feetOffsets[(int)gender * 9 + (int)race * 3 + (int)height],
             h
        );
    }

    /// <summary>
    /// Set sound sources for steps
    /// </summary>
    private void SetStepSoundEmitters()
    {
        AudioSource left = GameObject.Find("foot.L").AddComponent<AudioSource>();
        left.spatialBlend = 1;
        left.maxDistance = 4;
        left.minDistance = 3;
        left.outputAudioMixerGroup = stepAMG;
        left.playOnAwake = false;
        AudioSource right = GameObject.Find("foot.R").AddComponent<AudioSource>();
        right.spatialBlend = 1;
        right.maxDistance = 4;
        right.minDistance = 3;
        right.outputAudioMixerGroup = stepAMG;
        right.playOnAwake = false;
    }

    /// <summary>
    /// Set breathing components
    /// </summary>
    private void SetBreathingComponents()
    {
        // Breathing animator
        Animator breathingAnim = GameObject.Find("Suit").AddComponent<Animator>();
        breathingAnim.runtimeAnimatorController = Resources.Load("Components/BreathingController") as RuntimeAnimatorController;


    }

    /// <summary>
    /// Trigger exit routine
    /// </summary>
    public void ExitRoutine()
    {
        if (!exitRoutineStarted)
        {
            exitRoutineStarted = true;
        }
    }
}
