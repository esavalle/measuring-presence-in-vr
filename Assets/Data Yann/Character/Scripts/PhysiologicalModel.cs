﻿using UnityEngine;

/// <summary>
/// Computes physiological parameters in real time with speed / slope
/// </summary>
public class PhysiologicalModel : MonoBehaviour
{
    #region General parameters
    private bool initialized = false;
    private AnimationScript anim = null;
    private SetupAvatar setupAvatar;
    #endregion

    #region Avatar characteristics
    [Tooltip("Debug")]
    public bool debug = false;
    [Header("Avatar characteristics")]
    [Tooltip("Gender ('M' or 'F')")]
    private char gender = 'M';
    [Tooltip("Height (m)")]
    private float height = 1.75f;
    [Tooltip("Weight (kg)")]
    private float weight = 70;
    [Tooltip("Age (years)")]
    private float age = 30;
    [Tooltip("Fitness (0 for not fit, to 7 for fit)")]
    private float fitness = 3.5f;
    #endregion

    #region VO2
    [Tooltip("Min VO2 (mL/min/kg)")]
    public float VO2min;
    [Tooltip("Max VO2 (mL/min/kg)")]
    public float VO2max;
    [Header("VO2 (Read only)")]
    [Tooltip("Target VO2 (mL/min/kg) : corresponds to slope and speed")]
    public float VO2Activity = 20;
    [Tooltip("Current VO2 (mL/min/kg) : effort gradually reaches VO2 activity, smoother physiological parameter variation")]
    public float VO2Eq;
    #endregion

    #region Physiological parameters
    [Header("Current physiological parameters (read only)")]
    // Current rates
    public float currHR;
    public float currSV;
    public float currBR;
    public float currTV;
    #endregion

    #region Physiological parameter variation
    private float averageDT = 0.033f;           // Average frame delta time (value shouldn't change anything)
    private float varSpeed;                     // Alpha parameter for VO2Eq to reach VO2Activity
    private float lastPerVO2;                   // Average of VO2Eq in % used to increase / decrease recureration time,
                                                // if VO2Eq close to VO2max for a long time, longer recuperation time,
                                                // if VO2Eq close to VO2min for a long time, shorter recuperation time
    private float alphaLastPerVO2 = 0.1f;       // TODO : Aplha parameter used to update lastPerVO2
    private float minRecupFactor = 1;           // TODO : Minimum recuperation time multiplier (when lastPerVO2 = 0)
    private float maxRecupFactor = 4;           // TODO : Maximum recuperation time multiplier (when lastPerVO2 = 1)
    #endregion

    #region Character specific intermediary values
    // General
    private float BMI;          // Body mass index (kg/m²)
    private float perFat;       // Percentage of fat

    // HR related
    private float HRmin;        // Min heart rate (bpm)
    private float HRmax;        // Max heart rate (bpm)
    private float SVBP;         // Stroke volume (SV) bearing point (between 0.4 and 0.5 accroding to fitness)
                                // SV increased before SVBP, then is constant
    // Breathing related
    private float TVMin;        // Min tidal volume (L)
    private float TVMax;        // Max tidal volume (L)
    private float TVBP;         // Tidal volume breaking point (TV increased before TVBP, then is constant)
    private float TVPolyA;      // Tidal volume polunomial parameters for smooth computation
    private float TVPolyB;
    private float TVPolyC;
    private float TVPolyD;
    private float VEOverVO2min; // Min - ventilation over VO2
    private float VEOverVO2max; // Max - ventilation over VO2
    private float VEOverVO2BP;  // Ventilation over VO2 breaking point (between 0.55 and 0.65 according to fitness)
                                // Before VEOverVO2BP, VEOverVO2 = VEOverVO2min, then increases to VEOverVO2max
    private float BROffset;
    #endregion

    #region Fake update parameters
    private bool fakeUpdateVent = true;
    private float[] precompBR;
    private float[] precompTV = { 0, 0.5f, 0.8f, 0.9f, 0 };
    private float varSpeedFakeUpdate;
    private float[] zMarkers;
    private int currPrecompIdx = 0;
    #endregion

    public struct Card
    {
        public float HR;
        public float SV;
        public Card(float HR, float SV)
        {
            this.HR = HR;
            this.SV = SV;
        }
    }

    public struct Vent
    {
        public float BR;
        public float TV;
        public float TVMin;
        public float TVMax;
        public Vent(float BR, float TV, float TVMin, float TVMax)
        {
            this.BR = BR;
            this.TV = TV;
            this.TVMin = TVMin;
            this.TVMax = TVMax;
        }
    }

    /// <summary>
    /// Called once per frame
    /// - Update VO2
    /// - Compute recuperation factor
    /// - Update physiological parameters
    /// </summary>
    void Update()
    {
        // Get animation script
        if (anim == null)
            anim = gameObject.GetComponent<AnimationScript>();
        if (anim == null || !initialized)
            return;

        // Update target VO2 according to slope and speed
        if (!debug)
        {
            UpdateVO2Acti();

            // Update VO2Last (the longer and the more intense --> the more recup time)
            lastPerVO2 = lastPerVO2 * (1 - alphaLastPerVO2 * Time.deltaTime) + (VO2Eq - VO2min)
                / (VO2max - VO2min) * alphaLastPerVO2 * Time.deltaTime;

            // Set duration of variation
            float recupFactor = 1;
            if (VO2Eq > VO2Activity)
                recupFactor = minRecupFactor + (maxRecupFactor - minRecupFactor) * lastPerVO2;
            varSpeed = (1 - Mathf.Pow(0.05f, averageDT / setupAvatar.physioTimeTo95Per / recupFactor)) / averageDT;

            // Update parameters
            VO2Eq += (VO2Activity - VO2Eq) * varSpeed * Time.deltaTime;

            if (fakeUpdateVent) FakeUpdateVent();
        }

        // Update physiological parameters
        if (!fakeUpdateVent) UpdateVent(VO2Eq);
        UpdateCard(VO2Eq);
    }

    /// <summary>
    /// Compute all intermediary values at start of simulation
    /// Set activity to rest
    /// </summary>
    /// <param name="gender">Gender ('F', 'M')</param>
    /// <param name="height">Height in m</param>
    /// <param name="weight">Weight in kg</param>
    /// <param name="age">Age in yrs</param>
    /// <param name="fitness">Fitness on 0-7 scale</param>
    public void Initialize(char gender, float height, float weight, float age, float fitness, float[] precompBR)
    {
        // Get components
        setupAvatar = GameObject.Find("SINGLETON").GetComponent<SetupAvatar>();

        // Set parameters
        this.gender = gender;
        this.height = height;
        this.weight = weight;
        this.age = age;
        this.fitness = fitness;
        this.precompBR = precompBR;

        // Initialisation of intermediary values
        // General
        BMI = this.weight / (this.height * this.height);
        perFat = 1.39f * BMI + 0.16f * this.age - 10.34f * (this.gender == 'F' ? 0 : 1) - 9;
        VO2min = 3.6145f - 0.0367f * BMI - 0.0038f * this.age + 0.1790f * (this.gender == 'F' ? 1 : 2);
        VO2max = 50.513f + 1.589f * this.fitness - 0.289f * this.age - 0.552f * perFat + 5.863f
            * (this.gender == 'F' ? 0 : 1);
        // HR related
        HRmin = setupAvatar.HRmin;      // TODO
        HRmax = 220 - this.age;
        SVBP = 0.4f + this.fitness / 7 * 0.1f;
        // Breathing related
        TVMin = (this.gender == 'M' ? 0.6f : 0.5f);
        TVMax = -1.37f + 0.54f * (this.gender == 'M' ? 1 : 0) - 0.014f * this.age + 2.83f * this.height;
        // TVMax = (TVMax - TVMin) / setupAvatar.accBreathingFactor + TVMin;
        TVBP = 0.55f;                   // TODO
        TVPolyA = (TVMax - TVMin) * (1 / TVBP - 2);
        TVPolyB = (TVMax - TVMin) * (3 - 2 / TVBP);
        TVPolyC = (TVMax - TVMin) / TVBP;
        TVPolyD = TVMin;
        VEOverVO2BP = 0.55f + 0.1f / 7 * this.fitness;
        VEOverVO2min = 25;              // TODO
        VEOverVO2max = 37.5f;           // TODO
        BROffset = 10 - VEOverVO2min * VO2min / TVMin * weight * 0.001f;
        // FakeUpdate
        varSpeedFakeUpdate = (1 - Mathf.Pow(0.05f, averageDT / setupAvatar.physioTimeTo95Per)) / averageDT;
        zMarkers = new float[] {
            GameObject.Find("FlatToRamp1").transform.position.z,
            GameObject.Find("Ramp1ToRamp2").transform.position.z,
            GameObject.Find("Ramp2ToRamp3").transform.position.z,
            GameObject.Find("Ramp3ToFlat").transform.position.z
        };
        // Init current rates
        ForceRest();

        initialized = true;
    }

    /// <summary>
    /// Get current heart activity parameters
    /// </summary>
    /// <returns>[current heart rate (bpm), current stroke volume (0-1)]</returns>
    public Card GetCard() {
        return new Card(currHR, currSV);
    }

    /// <summary>
    /// Get current ventilation parameters
    /// </summary>
    /// <returns>[current breathing rate (bpm), current tidal volume (TVMin-TVMax), TVMin, TVMax]</returns>
    public Vent GetVent() {
        return new Vent(currBR, currTV, TVMin, TVMax);
    }

    /// <summary>
    /// Compute and update heart rate and stroke volume
    /// </summary>
    /// <param name="VO2">VO2 for cardiac parameters</param>
    private void UpdateCard(float VO2)
    {
        float perVO2 = (VO2 - VO2min) / (VO2max - VO2min);
        currHR = HRmin + perVO2 * (HRmax - HRmin);
        currSV = (perVO2 < SVBP) ? (perVO2 / SVBP) : 1;
    }

    /// <summary>
    /// Compute and update breathing rate and tidal volume
    /// </summary>
    /// <param name="VO2">VO2 for ventilation parameters</param>
    private void UpdateVent(float VO2)
    {
        float perVO2 = (VO2 - VO2min) / (VO2max - VO2min);
        float VEOverVO2 = (perVO2 < VEOverVO2BP) ? VEOverVO2min :
            (VEOverVO2min + (perVO2 - VEOverVO2BP) * (VEOverVO2max - VEOverVO2min) / (1 - VEOverVO2BP)); // VE/VO2
        float VE = VEOverVO2 * VO2 * weight * 0.001f; // L/min
        if (setupAvatar.physioTVSmooth)
            currTV = TVPolyA * Mathf.Pow(perVO2, 3) + TVPolyB * Mathf.Pow(perVO2, 2) + TVPolyC * perVO2 + TVPolyD;
        else
            currTV = (perVO2 < TVBP) ? (TVMin + (TVMax - TVMin) * perVO2 / TVBP) : TVMax;

        float tempTV = (currTV - TVMin) / setupAvatar.accBreathingFactor + TVMin;

        currBR = VE / tempTV + BROffset;
    }

    /// <summary>
    /// Update ventilation parameters with precomputed values
    /// </summary>
    private void FakeUpdateVent()
    {
        // Check position
        if (currPrecompIdx < 4 && transform.position.z < zMarkers[currPrecompIdx]) currPrecompIdx++;

        // Update parameters (more recuperation time if at end of slope)
        if (currPrecompIdx < 4)
        {
            currBR += (precompBR[currPrecompIdx] - currBR) * varSpeedFakeUpdate * Time.deltaTime;
            currTV += (TVMin + precompTV[currPrecompIdx] * (TVMax - TVMin) - currTV) * varSpeedFakeUpdate * Time.deltaTime;
        }
        else
        {
            currBR += (precompBR[currPrecompIdx] - currBR) * varSpeedFakeUpdate / 5 * Time.deltaTime;
            currTV += (TVMin + precompTV[currPrecompIdx] * (TVMax - TVMin) - currTV) * varSpeedFakeUpdate / 5 * Time.deltaTime;
        }
    }

    /// <summary>
    /// Force avatar to rest instantaneously
    /// </summary>
    public void ForceRest()
    {
        VO2Activity = VO2min;
        UpdateCard(VO2min);
        if (fakeUpdateVent)
        {
            currBR = precompBR[0];
            currTV = precompTV[0];
        }
        else UpdateVent(VO2min);
        VO2Eq = VO2min;
        lastPerVO2 = 0;
    }

    /// <summary>
    /// Rest avatar (set activity to VO2min)
    /// </summary>
    public void Rest() { VO2Activity = VO2min; }

    /// <summary>
    /// Updates VO2 activity according to slope and speed
    /// </summary>
    private void UpdateVO2Acti()
    {
        // Get slope (slope % = tan slope) and speed (1 unit ~= 3 km/h) information
        float slope = anim.slopeSine / anim.slopeCosine;
        float speed = anim.GetSpeed() * 3f;
        // Compute VO2 relative to slope and speed
        // TODO : divide it by 2.1 to limit its value but keep the trend
        if (slope < 0) slope = 0;
        VO2Activity = Mathf.Clamp(
            VO2min + (0.185f * speed + slope * 108f) / 2.3f,
            VO2min, VO2max);
    }
}
