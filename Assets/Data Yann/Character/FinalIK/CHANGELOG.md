# Changelog
## [2.0] - 2020-10-15

# Upgrade Guide
* MAKE A BACKUP! Open a new scene, delete “Plugins/RootMotion” and reimport. Also reimport PuppetMaster if you had that in your project.
* If you were using any of the integration packages, reimport them from “Plugins/RootMotion/FinalIK/_Integration”.
* TwistRelaxer.cs was restructured to support multiple relaxers on the same body part and TwistRelaxer components need to be set up again, sorry for the inconvenience! Find the TiwstRelaxer, add in one or more TwistSolvers, assign the Transforms, set weight to 1 and “Parent Child Crossfade” to 0.5 to get the previous default behaviour. If you wish to use multiple solvers, add them into the array in inverse hierarchical order - children first.

# Improvements
* Added VRIKCalibrator.CalibrateHead() and CalibrateHands() to make it easier to calibrate avatars with arbitrary bone orientations.
* Added VRIKCalibrationBasic.cs and “VRIK (Simple Head & Hands Calibration)” demo to show an easier way to calibrate VRIK for the most common head & hands VR case.
* Added support for additional AimIK pass for the head to FPSAiming.cs to support look-at while aimWeight less than 1.
* Added default pose and Start/Stop Solver buttons to EditorIK, improving it’s reliability and usability. Fixed EditorIK not working with LookAtIK.
* Added letting go with one hand functionality to PickUp2Handed.cs (InteractionSystem).
* TwistRelaxer.cs was restructured to support multiple relaxers on the same body part. See Upgrade Guide for more information.
* Added VRIK PUN demo (_Integration folder).
* Added VRIK PUN2 demo (_Integration folder).
* Added GetYaw(), GetPitch() and GetBank() methods to V3Tools.cs and QuaTools.cs.
* Added “Smooth Damp Time” to AimController.cs. This can be used as an alternative target direction interpolation method (disabled by default) to interpolate yaw, pitch and magnitude separately instead of spherical interpolation that can cause unwanted results sometimes.
* Added “Max Body Y Offset” to VRIK Locomotion settings. Reducing this value reduces head bob from locomotion.
* Updated SteamVR integration package to v2.6.1.
* Added “Three DOF” rotation mode to InteractionTarget to allow objects to be picked up from any angle.
* Added the “scale” parameter to VRIK to make it work consistently with small or large scale characters.
* Added VRIK.solver.locomotion.Relax() to force VRIK to take two footsteps to realign to a more neutral position when standing.
* Added CCDBendGoal.cs. Add this to a GameObject you wish CCD to bend towards.


# Fixes
* Fixed a bug with Grounding.lowerPelvisWeight in some cases lifting the pelvis and liftPelvisWeight lowering it.
* Fixed Baker IK position/rotation baking for Animators that have parent gameobjects.
* Fixed Baker not updating clip length when overwriting a file.
* Fixed GrounderBipedIK spine rolling when Animator was disabled.
* Added QueryTriggerInteraction.Ignore to all Grounder raycasts.
* Fixed VS warnings about redundant SerializeField attributes.
* Fixed InteractionObject.cs not using CompareTag.
* Fixed hands lagging behind when a moving player picked up an object with the Interaction System.
* Fixed VRIK not rotating the pelvis correctly when Pelvis Target set and Pelvis Rotation Weight set to 1.
* Simplified and optimized RotationLimitHinge calculations.
* Fixed InteractionEffector NullReferenceException when using Enter Play Mode Options.
* Fixed Grounding.isGrounded always returning true. Fixed GrounderIK rotating the root while not grounded.
* Fixed FingerRig solution dependence on hand rotation.
* Fixed VRIK rotating the upper arm bone even if arm Position/Rotation Weight is 0.

## [1.9] - 2019-09-09

For the full history of release notes, see FinalIK Change Log.pdf in the package.

# Upgrade Guide
* MAKE A BACKUP! Open a new scene, delete “Plugins/RootMotion” and reimport. Also reimport PuppetMaster if you had that in your project.
* If you were using any of the integration packages, reimport them from “Plugins/RootMotion/FinalIK/_Integration”.
* Dropped support for Unity 5.x (not supported by Asset Store anymore). Min supported Unity version now is 2017.4.28f LTS. If you need a Unity5.x compatible version, please contact developer. 

# Improvements
* Improved VRIK master weight blending.
* Added BendGoalWeight to InteractionObject.
* Added LOD and VRIKLODController.cs to VRIK. Setting LOD level to 1 saves approximately 20% of solving time. LOD level 2 means IK is culled, with only root position and rotation updated if locomotion enabled.
* Added “Root Heading Offset” to VRIK Spine settings. Enables you to change the angle of the root relative to the HMD to turn the avatar sideways if you’d like to have an angled stance for example when holding a rifle or boxing.
* Added SetFootPosition() to GroundingLeg.cs. Enables you to override the animated position of the foot and therefore use the single pass of IK already used by the Grounder to offset or plant a foot to a world space position together with the grounding effect.
* Changed MechSpiderLeg.cs to keep the feet rotated relative to ground normal.
* Added RotationLimit.SetDefaultLocalRotation(Quaternion localRotation) to allow you to define limit default local rotation by script.
* Updated Oculus integration package to v1.37.
* Updated SteamVR integration package to v2.2.0.
* Added EditorIK.cs to update any IK component in Editor mode.
* Added baseForwardOffsetEuler to the eyes of LookAtIK so clamping base direction could be adjusted for irregularly shaped eyes.
* Updated UMA integration package to v 2.8.1.
* Added the Baker, a powerful new tool for baking IK to Humanoid, Generic and Legacy animation clips.
* Added AimIK, FBBIK and LimbIK demos for the Baker.


# Fixes
* Fixed Grounder’s “Overstep Fall Down” functionality in Unity 2018, where raycasts starting from inside colliders return Vector3.zero as hit point.
* Fixed a bug in VRIKCalibrator that did not allow to calibrate without a pelvis target.
* Fixed arm localPosition warping when blending in/out of of VRIK weight.
* Fixed GrounderIK twitching with AnimatePhysics update mode.
* Fixed VRIK pelvis rotating to Pelvis Target if it assigned, but Pelvis Rotation Weight zero.
* Fixed FingerRig solution dependence on hand rotation.
* Fixed IKSolverTrigonometric/IKSolverLimb failing when solved on extremely small rigs.
* Fixed VRIK arm solver elbow twisting out of hinge limit.
* Fixed bugs in IKSolverLookAt.GetPoints() and IKSolverFullBody.GetPoints().
* Removed all warnings generated by Final IK scripts in Unity 2018.3.
* Moved InteractionObject initiation from Awake to Start to allow creating them in runtime.
* Fixed VRIK thigh and calf twist rotation for characters that have no toe bones.
* fixed RotationLimit scene view tools not showing up in Unity 2018 versions.
* Stopped VRIK locomotion from taking repeated steps to the same position.