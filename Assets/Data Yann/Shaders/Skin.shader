﻿Shader "Custom/Skin" {
    Properties{
        _Redness("Redness", Range(0,1)) = 0.0
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _MainTex2("Albedo 2 (RGB)", 2D) = "white" {}
        _Wetness("Wetness", Range(0,1)) = 0.0
        _Glossiness("Smoothness", Range(0,1)) = 0.5
        _GlossMap("GlossMap", 2D) = "white" {}
        _RedMask("RedMask", 2D) = "white" {}
        _NormalMap("NormalMap", 2D) = "withe" {}
        _NormalStrength("NormalStrength", Range(0,1)) = 0.0
    }
        SubShader{
            Tags { "RenderType" = "Fade" }
            LOD 200

            CGPROGRAM
            // Physically based Standard lighting model, and enable shadows on all light types
            //#pragma surface surf Standard fullforwardshadows
        
            #pragma surface surf SimpleSpecular
            #pragma target 3.5

            sampler2D _MainTex;
            sampler2D _MainTex2;
            sampler2D _GlossMap;
            sampler2D _RedMask;
            sampler2D _NormalMap;

            struct Input {
                float2 uv_MainTex;
                float2 uv_MainTex2;
                float2 uv_GlossMap;
                float2 uv_RedMask;
                float2 uv_NormalMap;
            };

            half _Redness;
            half _Glossiness;
            half _Wetness;
            half _NormalStrength;

            half4 LightingSimpleSpecular(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
                half3 h = normalize(lightDir + viewDir);

                half diff = max(0, dot(s.Normal, lightDir));

                float nh = max(0, dot(s.Normal, h));
                float spec = pow(nh, 48.0) * s.Gloss;

                half4 c;
                c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
                c.a = s.Alpha;
                return c;
            }

            void surf(Input IN, inout SurfaceOutput o) {
                float mask = tex2D(_RedMask, IN.uv_RedMask).r;
                // albedo
                fixed4 c = lerp(tex2D(_MainTex, IN.uv_MainTex), tex2D(_MainTex2, IN.uv_MainTex2), _Redness) * mask + (1 - mask) * tex2D(_MainTex, IN.uv_MainTex);
                o.Albedo = c.rgb;
                // gloss
                o.Gloss = dot(float4(0.3, 0.59, 0.11, 0), lerp(_Glossiness, tex2D(_GlossMap, IN.uv_GlossMap), _Wetness));
                // normal
                o.Normal = UnpackScaleNormal(tex2D(_NormalMap, IN.uv_NormalMap), _NormalStrength);
            }

            ENDCG
        }
        Fallback "Specular"
}
