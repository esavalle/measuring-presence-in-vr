Shader "Custom/InvisibleMask" {
    SubShader{
        // draw after all opaque objects:
        Tags { "Queue" = "Geometry+99" }
        Pass {
          Blend Zero One // keep the image behind it
        }
    }
}
