Shader "Custom/TerrainMix3_WeirdMaps"
{
    Properties
    {
        _Control("Control (RGBA)", 2D) = "red" {}
        _Splat0("Layer 0 (R)", 2D) = "white" {}
        _Occlusion0("Occlusion[G]/Spec[A] 0 (R)", 2D) = "white" {}
        _Normal0("Normal 0 (R)", 2D) = "white" {}
        _Stren0("Normal strength 0", Range(0,1)) = 1.0
        _Splat1("Layer 1 (G)", 2D) = "white" {}
        _Occlusion1("Occlusion[G]/Spec[A] 1 (G)", 2D) = "white" {}
        _Normal1("Normal 1 (R)", 2D) = "white" {}
        _Stren1("Normal strength 1", Range(0,1)) = 1.0
        _Splat2("Layer 2 (B)", 2D) = "white" {}
        _Occlusion2("Occlusion[G]/Spec[A] 2 (B)", 2D) = "white" {}
        _Normal2("Normal 2 (R)", 2D) = "white" {}
        _Stren2("Normal strength 2", Range(0,1)) = 1.0
    }

        SubShader
    {
            Tags
            {
                "Queue" = "Geometry-100"
                "RenderType" = "Opaque"
            }
        CGPROGRAM
        #pragma surface surf Lambert
        struct Input
        {
            float2 uv_Control : TEXCOORD0;
            float2 uv_Splat0 : TEXCOORD1;
        };

        sampler2D _Control;
        sampler2D _Splat0,_Splat1,_Splat2;
        sampler2D _Normal0, _Normal1, _Normal2;
        sampler2D _Occlusion0, _Occlusion1, _Occlusion2;
        half _Stren0, _Stren1, _Stren2;

        void surf(Input IN, inout SurfaceOutput o)
        {
            // control
            fixed4 splat_control = tex2D(_Control, IN.uv_Control);
            fixed3 norm_vec = splat_control.r + splat_control.g + splat_control.b;
            half norm_sca = splat_control.r + splat_control.g + splat_control.b;
            // color
            fixed3 col;
            col = splat_control.r * tex2D(_Splat0, IN.uv_Splat0).rgb;
            col += splat_control.g * tex2D(_Splat1, IN.uv_Splat0).rgb;
            col += splat_control.b * tex2D(_Splat2, IN.uv_Splat0).rgb;
            col /= norm_vec;
            // occlusion
            half occ;
            occ = splat_control.r * tex2D(_Occlusion0, IN.uv_Splat0).g;
            occ += splat_control.g * tex2D(_Occlusion1, IN.uv_Splat0).g;
            occ += splat_control.b * tex2D(_Occlusion2, IN.uv_Splat0).g;
            occ /= norm_sca;
            // specular
            half spe;
            spe = splat_control.r * tex2D(_Occlusion0, IN.uv_Splat0).a;
            spe += splat_control.g * tex2D(_Occlusion1, IN.uv_Splat0).a;
            spe += splat_control.b * tex2D(_Occlusion2, IN.uv_Splat0).a;
            spe /= norm_sca;
            // normal
            half3 norm;
            norm = UnpackScaleNormal(tex2D(_Normal0, IN.uv_Splat0), splat_control.r * _Stren0 / norm_vec);
            norm += UnpackScaleNormal(tex2D(_Normal1, IN.uv_Splat0), splat_control.g * _Stren1 / norm_vec);
            norm += UnpackScaleNormal(tex2D(_Normal2, IN.uv_Splat0), splat_control.b * _Stren2 / norm_vec);
            // end
            o.Albedo = col * occ;
            o.Specular = spe;
            o.Normal = norm;
            o.Alpha = 0.0;
        }
        ENDCG
    }

    // Fallback to Diffuse
    Fallback "Diffuse"
}