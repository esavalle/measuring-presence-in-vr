﻿﻿using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine;
using System;
using System.Threading;
using UnityEngine;

/// <summary>
/// TCP code example from: https://gist.github.com/danielbierwirth/0636650b005834204cb19ef5ae6ccedb
/// author: Jimmy Petit
/// MAJ: Mathis Fleury
/// </summary>
public class TCPWithOpenVibe : MonoBehaviour
{
    public int tcpPort;
    public string hostname;
    private TcpClient tcpSocket = new TcpClient();
    private bool socketConnected = false;
    private Thread tcpListenerThread;
    private Thread tcpConnectionThread;
    private bool tcpConnectionRun = false;
    private bool tcpListenerRun = false;

    private List<string> possibleIncommingStimulation;

    public ScenarioTest st;
    public GestionQuestionnaire gq;
    //SceneBehavior scene;
    public static string Decode;

    /// <summary> 	
	/// Connection callback
	/// </summary> 	
    private void TcpConnection()
    {
        while (tcpConnectionRun)
        {
            try
            {
                tcpSocket.Connect(hostname, tcpPort);
                socketConnected = true;
                break;
            }
            catch (Exception e)
            {
                //Debug.Log("TCPOpenViBE.tcpConnect exception: " + e);
            }
        }
        //Debug.Log("TCPOpenViBE.tcpConnect exit. tcpSocket.Connected =  " + tcpSocket.Connected);
    }

    /// <summary>
    /// Decode incomming data from OV.
    /// possibleIncommingStimulation is a list of possible OVTK stimulation from OV.
    /// If a stimulation received does not appear in the list, the program print the stimulation and throw an exception.
    /// </summary>
    private string DecodeIncommingData(int size, System.Byte[] incommingData, List<string> possibleIncommingStimulation)
    {
        string s = "";
        for (int i = 0; i < size; i++)
        {
            s += Convert.ToChar(incommingData[i]);
        }

        foreach (string stimulation in possibleIncommingStimulation)
            if (s.Contains(stimulation))
                return stimulation;

        throw new System.ArgumentException("Unrecognized stimulation: " + s);
    }

    /// <summary> 	
    /// Runs in background tcpListenerThread; Listens for incomming data. 	
	/// </summary>     
	public void Listener()
    {
        Byte[] bytes = new Byte[1024];
        while (tcpListenerRun)
        {
            try
            {
                // Get a stream object for reading
                int length;
                NetworkStream stream = tcpSocket.GetStream();
                // Read incomming stream into byte array.
                while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    var incomingData = new byte[length];
                    Array.Copy(bytes, 0, incomingData, 0, length);

                    string messageDecoded = DecodeIncommingData(length, incomingData, possibleIncommingStimulation);
                    Debug.Log("Message received: " + messageDecoded);
                    Decode = messageDecoded;
                    studyMessage(Decode);
                    
                }
            }
            catch (ArgumentException ae)
            {
                //Debug.Log("Socket exception: " + ae);
            }
        }
        //Debug.Log("Exit Listener Thread");
    }

    public void studyMessage(string Decode)
    {
        if (Decode.Contains("OVTK_StimulationId_Label_") && (Decode.Contains("00") || Decode.Contains("01")))
        {
            //sg.playSound(Decode.Contains("00"));
        }
        else if (Decode.Contains("OVTK_StimulationId_Label_02"))
        {
            st.induceBreakInPresence();
        }
        else if (Decode.Contains("OVTK_StimulationId_Label_11"))
        {
            gq.environnement = 1;
        }
        else if (Decode.Contains("OVTK_StimulationId_Label_12"))
        {
            gq.environnement = 2;
        }
        else if (Decode.Contains("OVTK_StimulationId_Label_13"))
        {
            gq.environnement = 3;
            Debug.Log("Activate BIP");
            st.constantBIP=true;
        }
        
        if (Decode.Contains("OVTK_GDF_"))
        {
            if (Decode.Contains("Start"))
            {
                //Debug.Log("Start of trial");
                st.startTrial();
            }
            else if (Decode.Contains("End"))
            {
               // Debug.Log("End of trial");
                st.stopTrial();
            }
        }
    }
    // Use this for initialization 	
    void Start()
    {
        // Fillup possibleIncommingStimulation, thanks to the number of targets in the scene.
        possibleIncommingStimulation = new List<string>();
        possibleIncommingStimulation.Add("OVTK_StimulationId_ExperimentStart");
        possibleIncommingStimulation.Add("OVTK_StimulationId_Label_00");
        possibleIncommingStimulation.Add("OVTK_StimulationId_Label_01");
        possibleIncommingStimulation.Add("OVTK_StimulationId_Label_02");
        possibleIncommingStimulation.Add("OVTK_StimulationId_Label_03");
        possibleIncommingStimulation.Add("OVTK_StimulationId_Label_11");
        possibleIncommingStimulation.Add("OVTK_StimulationId_Label_12");
        possibleIncommingStimulation.Add("OVTK_StimulationId_Label_13");
        possibleIncommingStimulation.Add("OVTK_StimulationId_RestStart");
        possibleIncommingStimulation.Add("OVTK_StimulationId_RestStop");
        possibleIncommingStimulation.Add("OVTK_StimulationId_ExperimentStop");
        possibleIncommingStimulation.Add("OVTK_GDF_Start_Of_Trial");
        possibleIncommingStimulation.Add("OVTK_GDF_End_Of_Trial");


        //Debug.Log("Possible incomming stimulation considered: ");
        foreach (string stimulation in possibleIncommingStimulation)
        {
            //Debug.Log(stimulation);
        }

        // Initiate TCP Client connection
        tcpConnectionThread = new Thread(new ThreadStart(TcpConnection));
        tcpConnectionRun = true;
        tcpConnectionThread.Start();

    }

    // Update is called once per frame
    void Update()
    {
        if (socketConnected == true)
        {
            if (tcpListenerThread == null || !tcpListenerThread.IsAlive)
            {
                tcpListenerThread = new Thread(new ThreadStart(Listener));
                tcpListenerThread.IsBackground = true;
                tcpListenerRun = true;
                tcpListenerThread.Start();

                //Debug.Log("TCPWithOpenViBE.Update: listener thread started!");
            }
        }
    }

    void OnDestroy()
    {
        if (tcpListenerThread != null && tcpListenerThread.IsAlive)
        {
            //Debug.Log("TCPWithOpenViBE.OnDestroy: stop tcpListenerThread");
            tcpListenerRun = false;
        }

        if (tcpConnectionThread != null && tcpConnectionThread.IsAlive)
        {
            //Debug.Log("TCPWithOpenViBE.OnDestroy: stop tcpConnectionThread");
            tcpConnectionRun = false;
        }
    }
}