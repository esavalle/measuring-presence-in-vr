using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UInteractCpt : UInteract
{
    public bool add = false;
    public GameObject textToModify;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public override void onClick()
    {
        int value = Int16.Parse(textToModify.GetComponent<UnityEngine.UI.Text>().text);
        if (add)
        {
            textToModify.GetComponent<UnityEngine.UI.Text>().text = (value + 1).ToString();
        }
        else
        {
            textToModify.GetComponent<UnityEngine.UI.Text>().text = (value - 1).ToString();
        }
        
    }
}
