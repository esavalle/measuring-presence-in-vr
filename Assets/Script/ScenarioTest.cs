using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;
using UnityEngine.UI;
using System;
using System.IO;

public class ScenarioTest : MonoBehaviour
{
    private int cpt;
    public GameObject bip;
    public bool constantBIP = false;
    public SetupAvatar_PreExp scriptAv;

    private float timeStartBIP;
    private bool BIPinProgress;
    public float BIPTime;
    private bool produceBIP;
    public HeadTracking ht;

    public GameObject renard;
    public GameObject champi;
    public int nbRenard;
    public int nbChampi;
    public GameObject spawnPosition;

    public GameObject controller;

    public GameObject comptage;
    private List<Transform> positions;
    private bool activateCanvas;
    public bool allRab;
    public bool trialStarted = false;
    public bool french = false;
    public int numPart;
    public UInteractValidate uival;
    public GestionQuestionnaire gestQ;
    public Text questionChampi;
    public Text questionChat;
    // Start is called before the first frame update
    void Start()
    {
        uival.subjectName = "Comptage" + numPart.ToString();
        gestQ.partNumber = numPart;
        if (french)
        {
            gestQ.startFrench();
            questionChampi.text = "Combien de champignons avez-vous compt� ?";
            questionChat.text = "Combien de chats avez-vous compt� ?";
        }
        controller.GetComponent<SteamVR_LaserPointer>().enabled=false;
        int nbChild = spawnPosition.transform.childCount;
        List<int> unAvailablePosition = new List<int>();
        if (allRab)
        {
            for (int i = 0; i < nbChild; i++)
            {
                Instantiate(renard, spawnPosition.transform.GetChild(i));
            }

        }
        else if(nbChild > nbRenard + nbChampi)
        {
            List<bool> usedPosition = new List<bool>(new bool[nbChild]);
            for (int i = 0; i < nbRenard; i++)
            {
                System.Random r = new System.Random();
                int val = -1;
                int cpt = 50;
                do
                {
                    val = r.Next(0, nbChild);
                    cpt = cpt - 1;
                } while (unAvailablePosition.Contains(val) == true && cpt > 0);
                unAvailablePosition.Add(val);
                Instantiate(renard, spawnPosition.transform.GetChild(val));
            }
            for (int i = 0; i < nbChampi; i++)
            {
                System.Random r = new System.Random();
                int val = -1;
                cpt = 50;
                do
                {
                    val = r.Next(0, nbChild);
                    cpt = cpt - 1;
                } while (unAvailablePosition.Contains(val) == true && cpt > 0);
                unAvailablePosition.Add(val);
                Instantiate(champi, spawnPosition.transform.GetChild(val));
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (constantBIP)
        {
            bip.SetActive(true);
        }
        if(BIPinProgress && Time.time > timeStartBIP + BIPTime)
        {
            BIPinProgress = false;
            bip.SetActive(false);
        }
        if (produceBIP)
        {
            produceBIP = false;
            BIPinProgress = true;
            bip.SetActive(true);
            timeStartBIP = Time.time;
        }
        if (activateCanvas)
        {
            controller.GetComponent<SteamVR_LaserPointer>().enabled = true;
            activateCanvas = false;
            if (comptage != null)
            {
                comptage.SetActive(true);
                ComptageInit ci = comptage.GetComponentInChildren<ComptageInit>();
                if (ci != null)
                {
                    ci.init();
                }
                else
                {
                    ci = comptage.GetComponent<ComptageInit>();
                    if (ci != null)
                    {
                        ci.init();
                    }
                }
            }
        }
        if (trialStarted && controller.activeSelf)
        {
            controller.SetActive(false);
        }
        if (!trialStarted && !controller.activeSelf)
        {
            controller.SetActive(true);
        }
    }

    public void startTrial()
    {
        if(scriptAv != null)
        {
            scriptAv.startMovement();
        }
        else
        {
            Debug.Log("Connect script Avatar!");
        }
        if(ht != null)
        {
            ht.expeStart();
        }
        trialStarted = true;
    }
    public void stopTrial()
    {
        Debug.Log("End of trial");
        if (scriptAv != null)
        {
            scriptAv.stopMovement();
        }
        else
        {
            Debug.Log("Connect script Avatar!");
        }
        if (ht != null)
        {
            ht.expeStop();
        }
        activateCanvas = true;
        trialStarted = false;
    }
    public void induceBreakInPresence()
    {
        produceBIP = true;
    }
}
