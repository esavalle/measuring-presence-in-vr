using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LookingAt : MonoBehaviour
{
    private bool lookedAt = false;
    private bool looked = false;
    public float timeAtFirstGlare = 0;
    public float timeRequired = 2f;
    public GameObject attachedObject;
    public Animator anim;
    public bool destroyOnGlare;
    public void GazeFocusChanged(bool hasFocus)
    {
        if (hasFocus)
        {
            lookedAt = true;
            timeAtFirstGlare = Time.time;
        }
        else
        {
            lookedAt = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(lookedAt && (Time.time > timeAtFirstGlare + timeRequired) && !looked)
        {
            looked = true;
            if (anim != null)
            {
                anim.SetBool("LookedAt", true);
            }
            if(attachedObject != null && destroyOnGlare)
            {
                Destroy(attachedObject);
            }
        }
        if(looked && !lookedAt)
        {
            if(anim != null)
            {
                anim.SetBool("LookAway", true);
            }
        }
    }
}
