using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class GestionQuestionnaire : MonoBehaviour
{
    public List<string> questions;
    public List<string> repNeg;
    public List<string> repPos;
    public List<string> questionsfr;
    public List<string> repNegfr;
    public List<string> repPosfr;
    public List<int> reponses;
    public GameObject self;
    public int partNumber;
    public Text questionsText;
    public Text repNegText;
    public Text repPosText;
    public int environnement;
    public bool french = false;
    private string path = "C:/Users/esavalle/Documents/Presence/Projet Unity/Eye-Tracking-main/Logs/Resultats/";

    private int questionAct = -1;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void startFrench()
    {
        french = true;
        questions = questionsfr;
        repNeg = repNegfr;
        repPos = repPosfr;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartQuestionnaire()
    {
        questionAct = 0;
        questionsText.text = questions[questionAct];
        repNegText.text = repNeg[questionAct];
        repPosText.text = repPos[questionAct];
    }
    public void validate(int val)
    {
        reponses.Add(val);
        if(reponses.Count == questions.Count)
        {
            //using (StreamWriter sw = File.CreateText(path + "ReponsesQuestionnaire"+partNumber + ".txt"))
            using (FileStream fs = new FileStream(path + "ReponsesQuestionnaire" + partNumber + ".txt", FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                string reps = ""+environnement.ToString()+":";
                for(int i = 0; i < reponses.Count; i++)
                {
                    reps = reps + reponses[i].ToString() + "/";
                }
                sw.WriteLine(reps);
            }
            self.SetActive(false);
        }
        else
        {
            questionAct = questionAct+1;
            questionsText.text = questions[questionAct];
            repNegText.text = repNeg[questionAct];
            repPosText.text = repPos[questionAct];
        }
    }
    
}
