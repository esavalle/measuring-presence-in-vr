using NAudio.Wave;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundGenerator : MonoBehaviour
{
    public AudioSource audioS;
    public AudioClip standard;
    public AudioClip deviant;

    private bool triggerPlaySound;
    private bool playDeviant;
    private IEnumerator coroutine;
    private int deviceID;
    Mp3FileReader readerStand;
    Mp3FileReader readerDev;
    WaveOut waveOutStand;
    WaveOut waveOutDev;

    // Start is called before the first frame update
    void Start()
    {
        readerStand = new Mp3FileReader("Assets/Sounds/1000_1ms.mp3");
        readerDev = new Mp3FileReader("Assets/Sounds/2000_1ms.mp3");
        waveOutStand = new WaveOut(); // or WaveOutEvent()
        waveOutDev = new WaveOut(); // or WaveOutEvent()

        deviceID = 1;
        waveOutStand.DeviceNumber = deviceID;
        waveOutStand.Init(readerStand);
        waveOutDev.DeviceNumber = deviceID;
        waveOutDev.Init(readerDev);

        playDeviant = false;
        triggerPlaySound = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (triggerPlaySound)
        {
            triggerPlaySound = false;

            if (playDeviant)
            {
                readerDev.Position = 0;
                waveOutDev.Init(readerDev);
                //audioS.PlayOneShot(deviant);
                waveOutDev.Play();
            }
            else
            {
                readerStand.Position = 0;
                //audioS.PlayOneShot(standard);
                waveOutDev.Init(readerStand);
                waveOutStand.Play();
            }
            
        }
    }

    public void playSound(bool p)
    {
        triggerPlaySound = true;
        playDeviant = p;
    }
}
