using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

public class HeadTracking : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform cameraT;
    public float offsetTrig;

    private float prevRecord;
    public float timeBetweenSample;

    private float x, y, z;

    public AudioClip bip;
    public AudioSource aS;
    public bool save = false;
    public bool playSound = false;
    public string fileLocation;
    private string path = "C:/Users/esavalle/Documents/Presence/Projet Unity/Eye-Tracking-main/Logs/";
    public float timeAtExpStart=-1000;
    private bool start;
   
    void Start()
    {
        prevRecord = 0;
        x = -1000;
        y = -1000;
        z = -1000;
        if (!File.Exists(fileLocation))
        {
            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(fileLocation))
            {
                sw.WriteLine("HeadMovement Log");
            }	
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            using (StreamWriter sw = File.AppendText(path + fileLocation))
            {
                sw.WriteLine("New trial");
            }
            save = true;
            playSound = false;
            timeAtExpStart = Time.time;
            start = false;
        }
        if (prevRecord + timeBetweenSample < Time.time)
        {
            prevRecord = Time.time;
            if (x != -1000)
            {
                float dist = distance(x, y, z, cameraT.rotation.x, cameraT.rotation.y, cameraT.rotation.z);
                //Debug.Log(dist);
                
                if(dist > offsetTrig)
                {
                    if (save && fileLocation != "" && timeAtExpStart != -1000)
                    {
                        Debug.Log("Write head movement");
                        string text = (Time.time - timeAtExpStart).ToString() + " : Movement " + Math.Round(dist, 4).ToString();
                        using (StreamWriter sw = File.AppendText(path+fileLocation))
                        {
                            sw.WriteLine(text);
                        }

                    }
                }
            }    
            x = cameraT.rotation.x;
            y = cameraT.rotation.y;
            z = cameraT.rotation.z;

        }
    }

    private float distance(float x1, float y1, float z1,float x2, float y2, float z2)
    {
        return Mathf.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1) + (z2 - z1) * (z2 - z1));
    }
    public static void WriteLog(string fileName, string line)
    {
        StreamWriter file = new StreamWriter(fileName,  true);
        file.WriteLine(line);
    }
    public void expeStart()
    {
        start = true;
    }
    public void expeStop()
    {
        save = true;
        playSound = false;
        using (StreamWriter sw = File.AppendText(path + fileLocation))
        {
            sw.WriteLine("End of trial");
        }
    }

}
