using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UInteractValidate : UInteract
{
    public GameObject cptL;
    public GameObject cptC;
    public ScenarioTest st;
    public GameObject canvasQuestionnaire;
    public GestionQuestionnaire gQ;
    public String subjectName;
    public bool french;
    private string path = "C:/Users/esavalle/Documents/Emile/Presence/Projet Unity/Eye-Tracking-main/Logs/Resultats/";
    

    public GameObject uiPanel;
    // Start is called before the first frame update
    void Start()
    {
        if (!File.Exists(path+subjectName + ".txt"))
        {
            // Create a file to write to.
            //using (StreamWriter sw = File.CreateText(path + subjectName+".txt"))
            using (FileStream fs = new FileStream(path + subjectName + ".txt", FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine("Comptage objets");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void onClick()
    {
        Debug.Log("Validate");
        int nbL = st.nbRenard;
        int nbC = st.nbChampi;
        int nL = Int16.Parse(cptL.GetComponent<UnityEngine.UI.Text>().text);
        int nC = Int16.Parse(cptC.GetComponent<UnityEngine.UI.Text>().text);

        //using (StreamWriter sw = File.CreateText(path + subjectName + ".txt"))
        using (FileStream fs = new FileStream(path + subjectName + ".txt", FileMode.Append, FileAccess.Write))
        using (StreamWriter sw = new StreamWriter(fs))
        {
            sw.WriteLine("Lapin " + nbL.ToString() + "/" + nL.ToString());
            sw.WriteLine("Champignon " + nbC.ToString() + "/" + nC.ToString());
        }
        canvasQuestionnaire.SetActive(true);
        gQ.StartQuestionnaire();
        uiPanel.SetActive(false);
    }
}
